# atlv - Tag Recommendations

While atlv tags are application-dependent,
serialization libraries may need default tags for types in standard libraries.
This provides recommendations for these tags to differentiate common generic types as needed.

## Quantities

Quantities naturally encode non-negative integers.
They also are a good fit for enumerated values.
Quantities are the most efficient encoding for any data of less than or equal to 63 bits on average.

### 0 - Unsigned Integers

Unsigned integer types come in a variety of precisions, usually a power of 2 bits.
These all should be encoded with the tag 0, but should accept any tag when decoding.

### 1 - Signed Integers

Signed integers are typically close to zero, and can be encoded with a zigzag encoding.
This encoding maps positive numbers `x` to their double `2x`, and negative numbers `x` to `-2x-1`.

### 2 - Booleans, Bits

Many languages provide a 1-bit boolean type, with the values `true` and `false` or `True` and `False`.

### 21 - Unicode Characters

[Unicode](https://unicode.org) is a standard for representing human-readable data.
When individual characters are representable in a language, they are more efficiently represented as quantities than single-character strings.

### 512..575 - Unsigned Binary Fixed Point

Fixed point numbers should encode the number of binary digits right of the decimal in the tag.
The recommended range handles 1 to 64 such digits with the tag `511 + n`.

### 576..639 - Signed Binary Fixed Point

Signed fixed point numbers should encode the number of binary digits right of the decimal in the tag.
The recommended range handles 1 to 64 such digits with the tag `575 + n`.

### 640..671 - times and dates

#### 640 - TAI seconds before 1977

[TAI](https://en.wikipedia.org/wiki/International_Atomic_Time) is a atomic time scale that tracks [proper time](https://en.wikipedia.org/wiki/Proper_time) on earth.

The encoded quantity `s` refers to the second ending `s` seconds before the 1977 TAI gravitational correction.
So these seconds are about 1 part per billion shorter than [641q](641-tai-seconds-after-1977) seconds.

This is an unsigned representation with unlimited range into the past.

Note that TAI is a continuous time scale, and drifts relative to UTC due to leap seconds.

#### 641 - TAI seconds after 1977

[TAI](https://en.wikipedia.org/wiki/International_Atomic_Time) is a atomic time scale that tracks [proper time](https://en.wikipedia.org/wiki/Proper_time) on earth.

The encoded quantity `s` refers to the second beginning `s` seconds after the 1977 TAI gravitational correction.

This is an unsigned representation with unlimited range into the future.
It requires 5 bytes (7 including the tag) to store the current time,
and will roll over to 6 bytes at 3074-05-23 12:18:08 TAI.

Note that TAI is a continuous time scale, and drifts relative to UTC due to leap seconds.

#### 661 - Unix Time

[Unix Time](https://en.wikipedia.org/wiki/Unix_time) counts the seconds since 1970-01-01 00:00:00 UTC, excluding leap seconds.

This is an unsigned representation with unlimited range into the future.
It requires 5 bytes (7 including the tag) to store the current time,
and will roll over to 6 bytes at 3067-05-23 12:16:00 UTC.

#### 662 - Unix Time milliseconds

[Unix Time](https://en.wikipedia.org/wiki/Unix_time) counts the seconds since 1970-01-01 00:00:00 UTC, excluding leap seconds.

This is an unsigned representation with unlimited range into the future.
It requires 6 bytes (8 including the tag) to store the current time,
and will roll over to 7 bytes during 2110-06-20 03:06:22 UTC.

## Binaries

### 0 - Byte Strings and Byte Arrays

Arrays of Bytes are binary values, and should thus be embedded in them with no coding.

### 6 - Floating Point Numbers

Encodes a single IEEE 754 floating point binary number with `8*len` bits.

### 21 - Unicode Strings

[Unicode](https://unicode.org) is a standard for representing human-readable data.
Unicode strings should be encoded with [UTF-8](https://en.wikipedia.org/wiki/UTF-8).

### 193..197 - Floating Point Number Arrays

Encodes an array of IEEE 754 floating point binary numbers.
The number of bytes per number is encoded in the tag as `192 + log2 n`.

| tag | format    |
|-----|-----------|
| 193 | binary16  |
| 194 | binary32  |
| 195 | binary64  |
| 196 | binary128 |
| 197 | binary256 |

### 640..671 - high precision times and dates

While second and millisecond resolution times fit nicely into a quantity,
high precision times with popular epochs use more than 56 bits, at which point encoding sizes of quantities and binaries become equal.

#### 641 - TAI nanoseconds after 1977

[TAI](https://en.wikipedia.org/wiki/International_Atomic_Time) is a atomic time scale that tracks [proper time](https://en.wikipedia.org/wiki/Proper_time) on earth.

The encoded quantity `s` refers to the nanosecond beginning `s` nanoseconds after the 1977 TAI gravitational correction.

This is an unsigned representation with unlimited range into the future.
It requires 8 bytes (11 including the tag and length) to store the current time,
and will roll over to 9 bytes on 2561-07-21 23:34:34 TAI.

Note that TAI is a continuous time scale, and drifts relative to UTC due to leap seconds.

### 2080..2087 - Bit Arrays

Bit arrays hold some number of bits, `n`, which are not guaranteed to to be a multiple of 8.
The tag should be `2080 + n%8`, which is to say, it encodes the length in bits modulo 8.

### - Large Integers

Large Integers are expected to encode more than 63 bits on average.

## Unions

### 2 - Either / Result / Sum / Maybe / Option

- 0 - Left / Err / InL / Nothing / None

  Nothing / None hold a zero length array (unit)

- 1 - Right / Ok / InR / Just / Some

### 304 - Internet Addresses

- 0 - raw host name
	- raw encoded in a `0 binary`
	- Unicode encoded in a `21 binary`
- 2 - Ethernet MAC address
	- encoded in a `0 quantity`?
- 4 - IPv4 address
	- encoded in a `0 quantity`
- 6 - IPv6 address
	- encoded in a `0 binary`

## Arrays

### 0 - Heterogeneous Lists

Arrays, Vectors, Unit, Tuples

### 1 - Sets

### 2 - Objects

Maps, Associative Arrays

### 3 - Complex Numbers

### 17 - Rational Numbers
